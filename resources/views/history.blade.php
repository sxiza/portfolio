@extends('layouts.master')

@section('content')
    @if(Session::has('feedback'))
        <div class="alert alert-info center">
            {{Session::get('feedback')}}
        </div>
    @endif
    @foreach($historyItems as $item)
    <div class="page-header main-info">
        <h3 class="strong">{{$item->timeframe}}<br><small class="info">{{$item->company}}</small></h3>
        <h6>{{$item->info}}</h6>
        @if(!empty($item->url))
            <div class="list-group">
              <a href="{{$item->url}}" target="_blank" class="list-group-item">
                <p class="list-group-item-text strong" >
                    <span class="glyphicon glyphicon-new-window link" aria-hidden="true"></span>
                    {{$item->company}}
                </p>
              </a>
            </div>
        @endif
        @if (Auth::check())
            <div class="main-info center">
                <a type="button" class="btn btn-info btn-md" title="Edit" href="{{route('historyItem.edit',$item->id)}}">
                    <span class="glyphicon glyphicon-edit"></span>
                </a>
                {!! Form::open(['url' => ['historyItem', $item->id], 'method' => 'delete', 'style' => 'display:inline;']) !!}
                    <button id="removeReg" type="submit" class="btn btn-danger btn-md" title="Remove">
                        <span class="glyphicon glyphicon-remove"></span>
                    </button>
                {!! Form::close() !!}
            </div>
        @endif
    </div>
    @endforeach
    @if (Auth::check())
        <div class="center main-info">
            <a class="btn btn-primary" href="{{route('historyItem.create')}}">Add historic item</a>
        </div>
    @endif
@endsection