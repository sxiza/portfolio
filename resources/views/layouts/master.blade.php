<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        @if (App\User::first())
            <title>{{App\User::first()->name.' '.App\User::first()->surname}}</title>
        @else
            <title>CV</title>
        @endif

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="{{ asset('bootstrap/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('ag/css/cv.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('prettyphoto/css/prettyPhoto.css') }}" rel="stylesheet" type="text/css">

        <script src="{{ asset('jquery/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('bootstrap/js/bootstrap.js') }}" type="text/javascript"></script>
        <script src="{{ asset('prettyphoto/js/jquery.prettyPhoto.js') }}" type="text/javascript"></script>

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
                @if (App\User::first())
                    <a class="navbar-brand" href="/" class="{{ Request::is('/') ? 'active' : '' }}">{{substr(App\User::first()->name,0,1).substr(App\User::first()->surname,0,1)}}</a>
                @else
                    <a class="navbar-brand" href="/" class="{{ Request::is('/') ? 'active' : '' }}">CV</a>
                @endif
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                @if (App\User::first())
                  <ul class="nav navbar-nav">
                    <li class="{{ Request::is('portfolio') ? 'active' : '' }}"><a href="{{ route('portfolio') }}">Portfolio <span class="sr-only">(current)</span></a></li>
                    <li class="{{ Request::is('accolades') ? 'active' : '' }}"><a href="{{ route('accolades') }}">Accolades</a></li>
                    <li class="{{ Request::is('history') ? 'active' : '' }}"><a href="{{ route('history') }}">History</a></li>
                  </ul>
                @endif
              <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li><a class="btn btn-default" href="{{ url('/login') }}">Login</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                    <li>
                        <a href="{{url('contact/create')}}" type="submit" class="btn btn-default">I'm interested!</a>
                    </li>               
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
        <div class="">
            @yield('content')
        </div>

        <script type="text/javascript" charset="utf-8">
            $(document).ready(function(){
                $("a[rel^='prettyPhoto']").prettyPhoto({
                    social_tools: false
                });
            });
        </script>
    </body>
</html>
