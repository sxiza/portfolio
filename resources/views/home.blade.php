@extends('layouts.master')

@section('content')
<div class="page-header center">
    @if(Session::has('feedback'))
        <div class="alert alert-info">
            {{Session::get('feedback')}}
        </div>
    @endif
    @if (!empty($info))
        <div class="profile-header-container">   
            <div class="profile-header-img">
                <img class="img-circle" src="{{ asset($info->image) }}" />
                <!-- badge -->
     <!--            <div class="rank-label-container">
                    <span class="label label-default rank-label">100 puntos</span>
                </div> -->
            </div>
        </div>
        <h1><strong>{{$info->company_name}}</strong><br><small>{{$info->sub_heading}}</small></h1>
    @endif
</div>
<div class="center main-info">
    @if (!empty($info))
        <h5>{{$info->info}}</h5>
    @endif
    @if (Auth::check() && !empty($info))
        <a class="btn btn-primary" href="{{route('promoInfo.edit',$info->id)}}">Edit promo info</a>
    @elseif (Auth::check() && empty($info))
        <a class="btn btn-primary" href="{{route('promoInfo.create')}}">Add promo info</a>
    @elseif (empty($info))
        <h4> Login to add promo info </h4>
    @endif
</div>
@endsection
