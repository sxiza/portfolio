@extends('layouts.master')

@section('content')
    @if(Session::has('feedback'))
        <div class="alert alert-info center">
            {{Session::get('feedback')}}
        </div>
    @endif
    @foreach($portfolioItems as $item)
    <div class="panel panel-default">
        <div class="panel-heading center"><h4 class="strong">{{$item->title}}</h4></div>
        <div class="panel-body">
            @if(!empty($item->link))
                <div class="list-group">
                  <a href="{{$item->link}}" target="_blank" class="list-group-item">
                    <p class="list-group-item-text strong">
                        <span class="glyphicon glyphicon-new-window link" aria-hidden="true" ></span>
                        {{$item->title}}
                    </p>
                  </a>
                </div>
            @endif
            <h5>{{$item->info}}</h5>
            <p class="info"><strong>Created with:</strong></p>
            <ul>
                @foreach($item->frameworks as $framework)
                <li class="info" style="font-weight:bold; font-family: Lucida Console;">{{$framework->framework->name}}</li>
                @endforeach
            </ul>
            <div class="row" style="display:flex; flex-wrap: wrap;">
                @foreach($item->images as $image)
                <div class="col-xs-6 col-md-3">
                    <a href="{{asset($image->url)}}" rel="prettyPhoto" class="thumbnail">
                        <img src="{{asset($image->url)}}">
                    </a>
                </div>
                @endforeach
            </div>
        </div>
        @if (Auth::check())
            <div class="main-info center">
                <a type="button" class="btn btn-info btn-md" title="Edit" href="{{route('portfolioItem.edit',$item->id)}}">
                    <span class="glyphicon glyphicon-edit"></span>
                </a>
                {!! Form::open(['url' => ['portfolioItem', $item->id], 'method' => 'delete', 'style' => 'display:inline;']) !!}
                    <button id="removeReg" type="submit" class="btn btn-danger btn-md" title="Remove">
                        <span class="glyphicon glyphicon-remove"></span>
                    </button>
                {!! Form::close() !!}
            </div>
        @endif
    </div>
    @endforeach
    @if (Auth::check())
        <div class="center main-info">
            <a class="btn btn-primary" href="{{route('portfolioItem.create')}}">Add portfolio item</a>
        </div>
    @endif
@endsection