@extends('layouts.master')

@section('content')
	<div class="panel panel-default">
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<div class="panel-body">
		    {!! Form::open(['url' => ['portfolioItem'], 'files' => 'true']) !!}
		    	<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
		    		{!! Form::label('title','Product or project name', ['for' => 'title']) !!}
		    		{!! Form::text('title', null, ['class' => 'form-control']) !!}
		    	</div>
		    	<div class="form-group{{ $errors->has('info') ? ' has-error' : '' }}">
		    		{!! Form::label('info','Your role in this project', ['for' => 'info']) !!}
		    		{!! Form::textarea('info', null, ['class' => 'form-control', 'rows' => '5']) !!}
		    	</div>
		    	<div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
		    		{!! Form::label('link','A link to this project', ['for' => 'link']) !!}
		    		{!! Form::text('link', null, ['class' => 'form-control']) !!}
		    	</div>
		    	<div class="form-group{{ $errors->has('frameworks') ? ' has-error' : '' }}">
		    		{!! Form::label('frameworks','Frameworks or languages used', ['for' => 'frameworks']) !!}
		    		{!! Form::select('frameworks[]', $frameworks, null, ['multiple' => true, 'class' => 'form-control']) !!}
		    	</div>
		    	<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
		    		{!! Form::label('images','Images of the project (you can upload multiple)', ['for' => 'images']) !!}
		    		{!! Form::file('images[]', ['class' => 'form-control','multiple' => 'multiple']) !!}
		    	</div>
		    	{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
		    	<a href="{{url()->previous()}}" class="btn btn-default">Cancel</a>
		    {!! Form::close() !!}
	    </div>
    </div>
@endsection