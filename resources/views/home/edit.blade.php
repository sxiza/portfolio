@extends('layouts.master')

@section('content')
	<div class="panel panel-default">
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<div class="panel-body">
		    {!! Form::open(['method' => 'put','url' => ['promoInfo',$info->id], 'files' => 'true']) !!}
		    	<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
		    		{!! Form::label('image','The promotional image', ['for' => 'image']) !!}
		    		{!! Form::file('image', ['class' => 'form-control']) !!}
		    		<div class="row">
		    			<div class="col-md-12">
		    				<img src="{{url($info->image)}}" class="thumbnail col-md-3" alt="..."/>
	    				</div>
	    			</div>
		    	</div>
		    	<div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
		    		{!! Form::label('company_name','Your company', ['for' => 'company']) !!}
		    		{!! Form::text('company_name', $info->company_name, ['class' => 'form-control']) !!}
		    	</div>
		    	<div class="form-group{{ $errors->has('sub_heading') ? ' has-error' : '' }}">
		    		{!! Form::label('sub_heading','The sub heading', ['for' => 'sub_heading']) !!}
		    		{!! Form::text('sub_heading', $info->sub_heading, ['class' => 'form-control']) !!}
		    	</div>
		    	<div class="form-group{{ $errors->has('info') ? ' has-error' : '' }}">
		    		{!! Form::label('info','A short introduction', ['for' => 'info']) !!}
		    		{!! Form::textarea('info', $info->info, ['class' => 'form-control', 'rows' => '5']) !!}
		    	</div>
		    	{!! Form::submit('Edit', ['class' => 'btn btn-primary']) !!}
		    	<a href="{{url()->previous()}}" class="btn btn-default">Cancel</a>
		    {!! Form::close() !!}
	    </div>
    </div>
@endsection