@extends('layouts.master')

@section('content')
	<div class="panel panel-default">
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<div class="panel-body">
		    {!! Form::open(['url' => 'promoInfo', 'files' => 'true']) !!}
		    	<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
		    		{!! Form::label('image','The promotional image', ['for' => 'image']) !!}
		    		{!! Form::file('image', ['class' => 'form-control']) !!}
		    	</div>
		    	<div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
		    		{!! Form::label('company_name','Your company', ['for' => 'company']) !!}
		    		{!! Form::text('company_name', null, ['class' => 'form-control']) !!}
		    	</div>
		    	<div class="form-group{{ $errors->has('sub_heading') ? ' has-error' : '' }}">
		    		{!! Form::label('sub_heading','The sub heading', ['for' => 'sub_heading']) !!}
		    		{!! Form::text('sub_heading', null, ['class' => 'form-control']) !!}
		    	</div>
		    	<div class="form-group{{ $errors->has('info') ? ' has-error' : '' }}">
		    		{!! Form::label('info','A short introduction', ['for' => 'info']) !!}
		    		{!! Form::textarea('info', null, ['class' => 'form-control', 'rows' => '5']) !!}
		    	</div>
		    	{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
		    {!! Form::close() !!}
	    </div>
    </div>
@endsection