<html>
<head>
    <style type="text/css">
        body {
            margin: 0;
            font: 14px/23px Arial;
        }

        #headerContainer {
            padding: 0;
        }

        #headerContainer a {
            font: 14px/23px Arial;
            text-decoration: none;
            color: #1abc9c;
        }

/*headerBackground begin*/
         .headerBackground{
           border-top:solid 5px black;
           /*should be one of the following...*/
         }
        ._cyan{
          background-color: #76b0e7;
        }

        ._green{
          background-color: #a8d472;
        }

        ._amber{
          background-color: #e4b26b;
        }

        ._red{
          background-color: #d26659;
        }
/*end headerBackground*/

        #footerContainer {
            padding: 0;
        }

        #footerBackground {
            padding: 20;
            background: #f1f1f1;
            border-bottom:solid 5px black;
        }

        #emailBody p {
            font: 14px/23px Arial;
            color: #2d2d2d;
        }

        #emailBody a {
            color: #0c75b1;
            text-decoration: none;;
        }

        #emailBody ol li {
            font: 14px/20px Arial;
            color: #2d2d2d;
        }

        #emailContainer{

        }

        .footerline {
            font: 10px/13px Arial;
            color: #0e594a;
        }

        .footerline a {
            text-decoration: none;
            color: #0e594a;
            font-weight: bold;
        }

        .sendTo {
            padding-top: 10px;
            padding-bottom: 10px;
            font: 9px/13px Arial;
            color:#888888;
            font-weight: bold;
            display: block;
        }

        .strong {
            font-weight:bold;
        }
    </style>
</head>
<body>
    @yield('content')
</body>