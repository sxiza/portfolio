@extends('emails.master')

@section('content')
	<h4 id="headerContainer" class="headerBackground"> You have received a contact interest hit from your portfolio website.</h4>
	<div id="emailBody">
		<dl>
			<dt class="strong"> Company: </dt>
			<dd> {{$contact->company_name}} </dd>
			<dt class="strong"> Name: </dt>
			<dd> {{$contact->name}} </dd>
			<dt class="strong"> Email: </dt>
			<dd> {{$contact->email}} </dd>
			<dt class="strong"> Contact number: </dt>
			<dd> {{$contact->contact_no}} </dd>
			<dt class="strong"> Comments: </dt>
			<dd> {{$contact->comments}} </dd>
		</dl>
	</div>
@endsection