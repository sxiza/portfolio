@extends('layouts.master')

@section('content')
	<div class="panel panel-default">
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<div class="panel-body">
		    {!! Form::open(['method' => 'put','url' => ['accolade', $accolade->id]]) !!}
		    	<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
		    		{!! Form::label('title','Accolade title', ['for' => 'title']) !!}
		    		{!! Form::text('title', $accolade->title, ['class' => 'form-control']) !!}
		    	</div>
		    	<div class="form-group{{ $errors->has('info') ? ' has-error' : '' }}">
		    		{!! Form::label('info','More info', ['for' => 'info']) !!}
		    		{!! Form::textarea('info', $accolade->info, ['class' => 'form-control', 'rows' => '5']) !!}
		    	</div>
		    	<div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
		    		{!! Form::label('url','An informative link to this accolade', ['for' => 'url']) !!}
		    		{!! Form::text('url', $accolade->url, ['class' => 'form-control']) !!}
		    	</div>
		    	
		    	{!! Form::submit('Edit', ['class' => 'btn btn-primary']) !!}
		    	<a href="{{url()->previous()}}" class="btn btn-default">Cancel</a>
		    {!! Form::close() !!}
	    </div>
    </div>
@endsection