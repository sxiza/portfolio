@extends('layouts.master')

@section('content')
	<div class="panel panel-default">
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<div class="panel-body">
		    {!! Form::open(['url' => 'contact']) !!}
		    	<div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
		    		{!! Form::label('company_name','Your company', ['for' => 'company']) !!}
		    		{!! Form::text('company_name', null, ['class' => 'form-control']) !!}
		    	</div>
		    	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
		    		{!! Form::label('name','Your name', ['for' => 'name']) !!}
		    		{!! Form::text('name', null, ['class' => 'form-control']) !!}
		    	</div>
		    	<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
		    		{!! Form::label('email','Your email', ['for' => 'email']) !!}
		    		{!! Form::email('email', null, ['class' => 'form-control']) !!}
		    	</div>
		    	<div class="form-group{{ $errors->has('contact_no') ? ' has-error' : '' }}">
		    		{!! Form::label('contact_no','Your contact number', ['for' => 'contact_no']) !!}
		    		{!! Form::text('contact_no', null, ['class' => 'form-control']) !!}
		    	</div>
		    	<div class="form-group{{ $errors->has('comments') ? ' has-error' : '' }}">
		    		{!! Form::label('comments','Please, tell me more', ['for' => 'comments']) !!}
		    		{!! Form::textarea('comments', null, ['class' => 'form-control', 'rows' => '5']) !!}
		    	</div>
		    	{!! Form::submit('Contact me!', ['class' => 'btn btn-primary']) !!}
		    {!! Form::close() !!}
	    </div>
    </div>
@endsection