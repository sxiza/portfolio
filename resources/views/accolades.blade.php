@extends('layouts.master')

@section('content')
    @if(Session::has('feedback'))
        <div class="alert alert-info center">
            {{Session::get('feedback')}}
        </div>
    @endif
    @foreach($accolades as $accolade)
    <div class="page-header main-info">
        <h3 class="strong">{{$accolade->title}}<br><small>{{$accolade->info}}</small></h3>
        @if (!empty($accolade->url))
            <div class="list-group">
              <a href="{{$accolade->url}}" target="_blank" class="list-group-item">
                <p class="list-group-item-text strong">
                    <span class="glyphicon glyphicon-new-window link" aria-hidden="true"></span>
                    {{$accolade->title}}
                </p>
              </a>
            </div>
        @endif
    </div>
    @if (Auth::check())
        <div class="main-info center">
            <a type="button" class="btn btn-info btn-md" title="Edit" href="{{route('accolade.edit',$accolade->id)}}">
                <span class="glyphicon glyphicon-edit"></span>
            </a>
            {!! Form::open(['url' => ['accolade', $accolade->id], 'method' => 'delete', 'style' => 'display:inline;']) !!}
                <button id="removeReg" type="submit" class="btn btn-danger btn-md" title="Remove">
                    <span class="glyphicon glyphicon-remove"></span>
                </button>
            {!! Form::close() !!}
        </div>
    @endif
    @endforeach
    @if (Auth::check())
        <div class="center main-info">
            <a class="btn btn-primary" href="{{route('accolade.create')}}">Add accolade</a>
        </div>
    @endif
@endsection