@extends('layouts.master')

@section('content')
	<div class="panel panel-default">
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		<div class="panel-body">
		    {!! Form::open(['url' => 'historyItem']) !!}
		    	<div class="form-group{{ $errors->has('timeframe') ? ' has-error' : '' }}">
		    		{!! Form::label('timeframe','Timeframe of work history', ['for' => 'timeframe']) !!}
		    		{!! Form::text('timeframe', null, ['class' => 'form-control']) !!}
		    	</div>
		    	<div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
		    		{!! Form::label('company','Company worked for', ['for' => 'info']) !!}
		    		{!! Form::text('company', null, ['class' => 'form-control']) !!}
		    	</div>
		    	<div class="form-group{{ $errors->has('info') ? ' has-error' : '' }}">
		    		{!! Form::label('info','More info', ['for' => 'info']) !!}
		    		{!! Form::textarea('info', null, ['class' => 'form-control', 'rows'=> '5']) !!}
		    	</div>
		    	<div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
		    		{!! Form::label('url','A url link to the company or project', ['for' => 'url']) !!}
		    		{!! Form::text('url', null, ['class' => 'form-control']) !!}
		    	</div>
		    	{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
		    	<a href="{{url()->previous()}}" class="btn btn-default">Cancel</a>
		    {!! Form::close() !!}
	    </div>
    </div>
@endsection