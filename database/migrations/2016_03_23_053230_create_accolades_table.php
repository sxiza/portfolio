<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccoladesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('accolades', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id')->index('user_idx');
			$table->string('title', 100)->nullable();
			$table->string('info', 1000)->nullable();
			$table->string('url')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('accolades');
	}

}
