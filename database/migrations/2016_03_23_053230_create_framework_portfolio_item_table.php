<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFrameworkPortfolioItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('framework_portfolio_item', function(Blueprint $table)
		{
			$table->integer('portfolio_item_id')->index('fk_portfolio_item_idx');
			$table->integer('framework_id')->index('fk_framework_idx');
			$table->primary(['portfolio_item_id','framework_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('framework_portfolio_item');
	}

}
