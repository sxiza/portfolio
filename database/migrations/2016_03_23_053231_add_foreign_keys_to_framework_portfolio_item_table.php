<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFrameworkPortfolioItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('framework_portfolio_item', function(Blueprint $table)
		{
			$table->foreign('framework_id', 'fk_framework')->references('id')->on('frameworks')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('portfolio_item_id', 'fk_portfolio_item_id')->references('id')->on('portfolio_items')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('framework_portfolio_item', function(Blueprint $table)
		{
			$table->dropForeign('fk_framework');
			$table->dropForeign('fk_portfolio_item_id');
		});
	}

}
