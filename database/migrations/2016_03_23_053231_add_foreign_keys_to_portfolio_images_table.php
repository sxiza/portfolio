<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPortfolioImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('portfolio_images', function(Blueprint $table)
		{
			$table->foreign('portfolio_item_id', 'fk_portfolio_item')->references('id')->on('portfolio_items')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('portfolio_images', function(Blueprint $table)
		{
			$table->dropForeign('fk_portfolio_item');
		});
	}

}
