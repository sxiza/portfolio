<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PortfolioImage extends Model
{
    public function portfolioItem()
    {
    	$this->belongsTo('App\Models\PortfolioItem');
    }
}
