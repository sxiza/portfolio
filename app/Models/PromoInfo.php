<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoInfo extends Model
{
	public static $snakeAttributes = false;

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
