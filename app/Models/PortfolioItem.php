<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PortfolioItem extends Model
{
    public function images()
    {
    	return $this->hasMany('App\Models\PortfolioImage');
    }

    public function frameworks()
    {
    	return $this->hasMany('App\Models\FrameworkPortfolioItem');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
