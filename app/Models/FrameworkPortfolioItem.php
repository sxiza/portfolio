<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FrameworkPortfolioItem extends Model
{
    protected $table = "framework_portfolio_item";

    public function framework()
    {
    	return $this->belongsTo('App\Models\Framework');
    }

    public function portfolioItem()
    {
    	return $this->belongsTo('App\Models\PortfolioItem');
    }
}
