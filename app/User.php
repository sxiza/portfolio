<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'surname', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function portfolioItems()
    {
        return $this->hasMany('App\Models\PortfolioItem');
    }

    public function accolades()
    {
        return $this->hasMany('App\Models\Accolade');
    }

    public function historyItems()
    {
        return $this->hasMany('App\Models\HistoryItem');
    }

    public function contacts()
    {
        return $this->hasMany('App\Models\Contact');
    }

    public function promoInfo()
    {
        return $this->hasOne('App\Models\PromoInfo');
    }
}
