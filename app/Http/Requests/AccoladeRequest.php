<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Accolade;
use Auth;

class AccoladeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (!$this->route('accolade'))
        {
            return true;
        }

        return Accolade::where('id',$this->route('accolade'))
                        ->where('user_id',Auth::id())
                        ->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'info' => 'required'
        ];
    }
}
