<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\HistoryItem;
use Auth;

class HistoryItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (!$this->route('historyItem'))
        {
            return true;
        }

        return HistoryItem::where('id', $this->route('historyItem'))
                        ->where('user_id', Auth::id())
                        ->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'timeframe' => 'required',
            'company' => 'required',
            'info' => 'required'
        ];
    }
}
