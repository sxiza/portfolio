<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;
use App\Models\PortfolioItem;

class PortfolioItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (!$this->route('portfolioItem'))
        {
            return true;
        }

        return PortfolioItem::where('id', $this->route('portfolioItem'))
                        ->where('user_id', Auth::id())
                        ->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'info' => 'required',
            'frameworks' => 'required'
        ];
    }
}
