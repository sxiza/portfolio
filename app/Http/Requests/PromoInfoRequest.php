<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\PromoInfo;
use Auth;

class PromoInfoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (!$this->route('promoInfo'))
        {
            return true;
        }
        
        return PromoInfo::where('id', $this->route('promoInfo'))
                        ->where('user_id', Auth::id())
                        ->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required',
            'sub_heading' => 'required',
            'info' => 'required'
        ];
    }
}
