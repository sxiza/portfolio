<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Contact;
use App\User;
use Validator;
use Input;
use Redirect;
use Session;
use Mail;
use Exception;
use Log;

class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth', ['except' => 'create']);
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contact.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'company_name' => 'required',
            'name' => 'required',
            'email' => 'required|email',
            'comments' => 'required'
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::to('contact/create')
                ->withErrors($validator)
                ->withInput(Input::all());
        }
        else
        {
            $contact = new Contact;
            $contact->company_name = Input::get('company_name');
            $contact->name = Input::get('name');
            $contact->email = Input::get('email');
            $contact->contact_no = Input::get('contact_no');
            $contact->comments = Input::get('comments');
            User::first()->contacts()->save($contact);

            try
            {
                Mail::send('emails.contact',['contact' => $contact], function ($message) use ($contact){
                    $message->from($contact->email, $contact->company_name);
                    $message->to(User::first()->email, User::first()->name);
                    $message->subject("Portfolio contact interest");
                });
            }
            catch(Exception $e){
                Log::error($e . "\n");
            }
        }

        Session::flash('feedback',"Thanks, I'll make sure to get back to you.");
        return Redirect::to('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
