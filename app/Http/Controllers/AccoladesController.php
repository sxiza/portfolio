<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Http\Requests\AccoladeRequest;
use App\Models\Accolade;
use Auth;
use Session;
use Redirect;

class AccoladesController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$accolades = User::first()->accolades;
    	return view('accolades',['accolades' => $accolades]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('accolade.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AccoladeRequest $request)
    {
    	$accolade = new Accolade;
    	$accolade->title = $request->input('title');
    	$accolade->info = $request->input('info');
    	$accolade->url = $request->input('url');
    	Auth::user()->accolades()->save($accolade);

        Session::flash('feedback',"Accolade created");
        return Redirect::to('accolades');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $accolade = Accolade::find($id);
        return view('accolade.edit',['accolade' => $accolade]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AccoladeRequest $request, $id)
    {
        $accolade = Accolade::find($id);
        $accolade->title = $request->input('title');
        $accolade->info = $request->input('info');
        $accolade->url = $request->input('url');
       	$accolade->save();
        
        Session::flash('feedback',"Accolade changed");
        return Redirect::to('accolades');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $accolade = Accolade::find($id);

        if ($accolade->user_id == Auth::id())
        {
            $accolade->delete();
        }

        Session::flash('feedback',"Accolade deleted");
        return Redirect::to('accolades');
    }
}
