<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PortfolioItemRequest;
use App\User;
use App\Models\PortfolioItem;
use App\Models\PortfolioImage;
use App\Models\Framework;
use App\Models\FrameworkPortfolioItem;
use Auth;
use Session;
use Redirect;

class PortfolioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (User::count() == 0)
        {
            return Redirect::to('register');
        }
        else
        {
            $portfolioItems = User::first()->portfolioItems;

            return view('portfolio',['portfolioItems' => $portfolioItems]);
        } 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $frameworks = Framework::all()->lists('name','id');
        return view('portfolio_item.create', ['frameworks' => $frameworks]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PortfolioItemRequest $request)
    {
        $nbr = count($request->file('images')) - 1;
        foreach(range(0, $nbr) as $index) {
            $rules['images.' . $index] = 'image|max:4000';
        }

        $this->validate($request, $rules);

        $portfolioItem = new PortfolioItem;
        $portfolioItem->user()->associate(Auth::user());
        $portfolioItem->title = $request->input('title');
        $portfolioItem->info = $request->input('info');
        $portfolioItem->link = $request->input('link');
        $portfolioItem->save();

        foreach($request->input('frameworks') as $fID)
        {
            $framework = Framework::find($fID);
            $frameworkPortfolioItem = new FrameworkPortfolioItem;
            $frameworkPortfolioItem->framework()->associate($framework);
            $frameworkPortfolioItem->portfolioItem()->associate($portfolioItem);
            $frameworkPortfolioItem->save();
        }

        if ($request->file('images')[0] != null)
        {
            foreach($request->file('images') as $image)
            {
                $destinationPath = 'ag/images/'; // upload path
                $extension = $image->getClientOriginalExtension(); // getting image extension
                $fileName =  rand(11111,99999) . '.' . $extension; // renameing image
                $image->move($destinationPath, $fileName); // uploading file to given path

                $portfolioImage = new PortfolioImage;
                $portfolioImage->url = $destinationPath . $fileName;

                $portfolioItem->images()->save($portfolioImage);
            }
        }
        
        Session::flash('feedback',"Portfolio item created");
        return Redirect::to('portfolio');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $portfolioItem = PortfolioItem::find($id);
        $frameworks = Framework::all()->lists('name','id');
        // dd($portfolioItem->frameworks()->lists('framework_id')->all());
        return view('portfolio_item.edit',['portfolioItem' => $portfolioItem, 'frameworks' => $frameworks]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PortfolioItemRequest $request, $id)
    {
        $portfolioItem = PortfolioItem::find($id);
        $portfolioItem->title = $request->input('title');
        $portfolioItem->info = $request->input('info');
        $portfolioItem->link = $request->input('link');
        $portfolioItem->save();

        if ($request->file('images')[0] != null)
        {
            $nbr = count($request->file('images')) - 1;
            foreach(range(0, $nbr) as $index) {
                $rules['images.' . $index] = 'image|max:4000';
            }

            $this->validate($request, $rules);

            $portfolioItem->images()->delete();
            foreach($request->file('images') as $image)
            {
                $destinationPath = 'ag/images/'; // upload path
                $extension = $image->getClientOriginalExtension(); // getting image extension
                $fileName =  rand(11111,99999) . '.' . $extension; // renameing image
                $image->move($destinationPath, $fileName); // uploading file to given path

                $portfolioImage = new PortfolioImage;
                $portfolioImage->url = $destinationPath . $fileName;

                $portfolioItem->images()->save($portfolioImage);
            }
        }

        $portfolioItem->frameworks()->delete();
        foreach($request->input('frameworks') as $fID)
        {
            $framework = Framework::find($fID);
            $frameworkPortfolioItem = new FrameworkPortfolioItem;
            $frameworkPortfolioItem->framework()->associate($framework);
            $frameworkPortfolioItem->portfolioItem()->associate($portfolioItem);
            $frameworkPortfolioItem->save();
        }
        
        Session::flash('feedback',"Portfolio item changed");
        return Redirect::to('portfolio');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $portfolioItem = PortfolioItem::find($id);

        if ($portfolioItem->user_id == Auth::id())
        {
            $portfolioItem->delete();
        }

        Session::flash('feedback',"Portfolio item deleted");
        return Redirect::to('portfolio');
    }
}
