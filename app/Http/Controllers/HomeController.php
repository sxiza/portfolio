<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\PromoInfoRequest;
use App\Models\PromoInfo;
use App\User;
use Auth;
use Mail;
use Exception;
use Log;
use Session;
use Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'index']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (User::count() == 0)
        {
            return Redirect::to('register');
        }
        else
        {
            $promoInfo = User::first()->promoInfo;
            return view('home', ['info' => $promoInfo]);
        }     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('home.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PromoInfoRequest $request)
    {
        $rules = [
            'image' => 'required'
        ];

        $this->validate($request, $rules);

        $destinationPath = 'ag/images/'; // upload path
        $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
        $fileName =  rand(11111,99999) . '.' . $extension; // renameing image
        $request->file('image')->move($destinationPath, $fileName); // uploading file to given path

        $promoInfo = new PromoInfo;
        $promoInfo->user()->associate(Auth::user());
        $promoInfo->company_name = $request->input('company_name');
        $promoInfo->sub_heading = $request->input('sub_heading');
        $promoInfo->info = $request->input('info');
        $promoInfo->image = $destinationPath . $fileName;
        $promoInfo->save();

        Session::flash('feedback',"Promo info created");
        return Redirect::to('/');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $promoInfo = PromoInfo::find($id);
        return view('home.edit',['info' => $promoInfo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PromoInfoRequest $request, $id)
    {
        
        $promoInfo = PromoInfo::find($id);

        if ($request->file('image'))
        {
            $destinationPath = 'ag/images/'; // upload path
            $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            $fileName =  rand(11111,99999) . '.' . $extension; // renameing image
            $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
            $promoInfo->image = $destinationPath . $fileName;
        }
        
        $promoInfo->company_name = $request->input('company_name');
        $promoInfo->sub_heading = $request->input('sub_heading');
        $promoInfo->info = $request->input('info');
        $promoInfo->save();

        Session::flash('feedback',"Promo info changed");
        return Redirect::to('/');
    }
}
