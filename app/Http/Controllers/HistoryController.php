<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Models\HistoryItem;
use App\Http\Requests\HistoryItemRequest;
use Auth;
use Session;
use Redirect;

class HistoryController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$historyItems = User::first()->historyItems;
    	return view('history',['historyItems' => $historyItems]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('history_item.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HistoryItemRequest $request)
    {
    	$historyItem = new HistoryItem;
    	$historyItem->timeframe = $request->input('timeframe');
    	$historyItem->info = $request->input('info');
    	$historyItem->company = $request->input('company');
    	$historyItem->url = $request->input('url');
    	Auth::user()->historyItems()->save($historyItem);

        Session::flash('feedback',"Historic item created");
        return Redirect::to('history');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $historyItem = HistoryItem::find($id);
        return view('history_item.edit',['historyItem' => $historyItem]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HistoryItemRequest $request, $id)
    {
        $historyItem = HistoryItem::find($id);
        $historyItem->timeframe = $request->input('timeframe');
    	$historyItem->info = $request->input('info');
    	$historyItem->company = $request->input('company');
    	$historyItem->url = $request->input('url');
       	$historyItem->save();
        
        Session::flash('feedback',"Historic item changed");
        return Redirect::to('history');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $historyItem = HistoryItem::find($id);

        if ($historyItem->user_id == Auth::id())
        {
            $historyItem->delete();
        }

        Session::flash('feedback',"Historic item deleted");
        return Redirect::to('history');
    }
}
