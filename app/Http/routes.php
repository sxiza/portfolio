<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    //Home / Promotional info
    Route::get('/', ['uses' => 'HomeController@index', 'as' => 'home']);
    Route::resource('promoInfo', 'HomeController');

    //Portfolio items
	Route::get('portfolio', ['uses' => 'PortfolioController@index', 'as' => 'portfolio']);
	Route::resource('portfolioItem', 'PortfolioController');

	//Accolades
	Route::get('accolades', ['uses' => 'AccoladesController@index', 'as' => 'accolades']);
	Route::resource('accolade', 'AccoladesController');

	//History
	Route::get('history', ['uses' => 'HistoryController@index', 'as' => 'history']);
	Route::resource('historyItem', 'HistoryController');

	Route::resource('contact', 'ContactController');
    // Route::get('/', 'HomeController@index');
});
